function test(testObject){
    let array =[];
    for(let prop in testObject){
        array.push(testObject[prop]);
    }
    let mapped = array.map(function(word){
        return word[0].toUpperCase() + word.slice(1).toLowerCase();

    }).join(" ");
    return mapped;;
}

module.exports = test;